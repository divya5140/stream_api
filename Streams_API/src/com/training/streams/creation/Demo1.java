package com.training.streams.creation;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class Demo1 {
	
	public static void main(String[] args) {
		
		// stream.of
		Stream<Integer> stream=Stream.of(10,20,30,40,50);
		stream.forEach(System.out::println);
		
		//stream.of(array)
		
		Stream<String> stream1=Stream.of(new String[]{"1","2","3"});
		stream1.forEach(n -> System.out.println(n));
		
		
		//List.stream()
		
		List<String> list=Arrays.asList("rahul","priya","suma");
		list.stream().forEach(System.out::println);
		
		//stream.generate
		
		Stream<String> stream3=Stream.generate(()-> "stream").limit(4);
		stream3.forEach(System.out::println);
		
		
		Stream<Integer> stream4=Stream.iterate(10,n ->n-3).limit(3);
		stream4.forEach(System.out::println);
		
		
		
		
		Stream.Builder<String> stream5=Stream.builder();
		stream5.add("divya")
				.add("priya").add("sk").build().forEach(System.out::println);
		
		
				
	}

}
