package com.training.streams.intermediate.operation;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class FlatMap {

public static void main(String[] args) {
	
	
	List<Integer> s1=Arrays.asList(10,15,18,17);
	List<Integer> s2=Arrays.asList(30,55,98,27);
	List<Integer> s3=Arrays.asList(30,45);
	
	
	List<List<Integer>> resultList=Arrays.asList(s1,s2,s3);
	
	List<Integer> finalList=resultList.stream()
	.flatMap(list->list.stream()).filter(n -> n % 2!=0).collect(Collectors.toList());
	System.out.println(finalList);
	
	
}



}
