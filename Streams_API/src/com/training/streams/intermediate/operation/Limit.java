package com.training.streams.intermediate.operation;

import java.util.ArrayList;
import java.util.List;

public class Limit {

	
	
	public static void main(String[] args) {
		
		List<Integer> numList=new ArrayList<>();
		for(int i=0;i<15;i++)
		{
			numList.add(i);
		}
		numList.stream().limit(7).forEach(System.out::println);
	}
}
