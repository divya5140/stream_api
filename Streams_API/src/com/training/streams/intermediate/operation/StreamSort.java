package com.training.streams.intermediate.operation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.training.streams.Employee;

public class StreamSort {

public static void main(String[] args) {
	List<Integer> s1=Arrays.asList(10,15,18,17);
	s1.stream().sorted(Collections.reverseOrder()).forEach(System.out::println);
	s1.stream().sorted().forEach(System.out::println);
		
		
		List<Employee> employees=new ArrayList<>();
		employees.add(new Employee(1, "divya", 20, "female", 600000.00, 3));
		employees.add(new Employee(2, "kumar", 55, "male", 800000.00, 5));
		employees.add(new Employee(3, "manjula", 45, "female", 700000.00, 4));
		employees.add(new Employee(4, "sunil", 35, "male", 900000.00, 4));
		employees.add(new Employee(5, "suma", 32, "female", 800000.00, 3));
		employees.add(new Employee(6, "vishnu", 3, "male", 100000.00, 2));
		
		employees.stream()
		.sorted(Comparator.comparing(Employee::getSalary))
		//.sorted(Comparator.comparing(Employee::getSalary).reversed())
		.forEach(emp -> System.out.println(emp));
}
}