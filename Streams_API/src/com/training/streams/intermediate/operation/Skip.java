package com.training.streams.intermediate.operation;

import java.util.ArrayList;
import java.util.List;

public class Skip {
public static void main(String[] args) {
		
		List<Integer> numList=new ArrayList<>();
		for(int i=0;i<15;i++)
		{
			numList.add(i);
		}
		numList.stream().skip(5).forEach(System.out::println);
	}
}
