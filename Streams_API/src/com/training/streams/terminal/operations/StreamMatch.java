package com.training.streams.terminal.operations;

import java.util.ArrayList;
import java.util.List;

import com.training.streams.Employee;

public class StreamMatch {

	
public static void main(String[] args) {
		
		
		List<Employee> employees=new ArrayList<>();
		employees.add(new Employee(1, "divya", 20, "female", 600000.00, 3));
		employees.add(new Employee(2, "kumar", 55, "male", 800000.00, 5));
		employees.add(new Employee(3, "manjula", 45, "female", 700000.00, 4));
		employees.add(new Employee(4, "sunil", 35, "male", 900000.00, 4));
		employees.add(new Employee(5, "suma", 32, "female", 800000.00, 3));
		employees.add(new Employee(6, "vishnu", 3, "male", 100000.00, 2));
		
		boolean emp=employees.stream()
		//.allMatch(e -> e.getSalary() > 50000);
				//.anyMatch(e -> e.getAge()>50);
				.noneMatch(e -> e.getId()<7);
		System.out.println(emp);
		
}
}
