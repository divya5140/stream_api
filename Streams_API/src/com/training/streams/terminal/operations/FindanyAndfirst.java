package com.training.streams.terminal.operations;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class FindanyAndfirst {
public static void main(String[] args) {
	
	List<String> fruitList=Arrays.asList("Apple","mango","banana","Apple","orange");
//Optional<String> result=fruitList.stream().findAny();
	Optional<String> result=fruitList.stream().findFirst();
System.out.println(result.get());
}
}
