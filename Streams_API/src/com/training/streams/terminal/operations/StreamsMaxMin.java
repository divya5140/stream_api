package com.training.streams.terminal.operations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import com.training.streams.Employee;

public class StreamsMaxMin {
	
public static void main(String[] args) {
	List<Integer> s1=Arrays.asList(10,15,18,17);
		
	Optional<Integer> val=s1.stream().max(Comparator.comparing(Integer::intValue));
	System.out.println(val.get());
	Optional<Integer> vals=s1.stream().min(Comparator.comparing(Integer::intValue));
	System.out.println(vals.get());
	
	List<String> s2=Arrays.asList("App","Mango");
	s2.stream().max(Comparator.comparing(String::valueOf)).ifPresent(System.out::println);;
	
	
		List<Employee> employees=new ArrayList<>();
		employees.add(new Employee(1, "divya", 20, "female", 600000.00, 3));
		employees.add(new Employee(2, "kumar", 55, "male", 800000.00, 4));
		employees.add(new Employee(3, "manjula", 45, "female", 700000.00, 4));
		employees.add(new Employee(4, "sunil", 35, "male", 900000.00, 4));
		employees.add(new Employee(5, "suma", 32, "female", 800000.00, 2));
		employees.add(new Employee(6, "vishnu", 3, "male", 100000.00, 5));
		
		
		
				employees.stream()
		//.max(Comparator.comparing(Employee :: getRating))
		.min(Comparator.comparing(Employee :: getRating))
		.ifPresent(System.out::println);
			
				

}
}