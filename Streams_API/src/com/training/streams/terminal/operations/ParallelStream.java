package com.training.streams.terminal.operations;

import java.util.ArrayList;
import java.util.List;

import com.training.streams.Employee;

public class ParallelStream {
	
	public static void main(String[] args) {
	
			
		
		
			List<Employee> employees=new ArrayList<>();
			employees.add(new Employee(1, "divya", 20, "female", 600000.00, 3));
			employees.add(new Employee(2, "kumar", 55, "male", 800000.00, 4));
			employees.add(new Employee(3, "manjula", 45, "female", 700000.00, 4));
			employees.add(new Employee(4, "sunil", 35, "male", 900000.00, 4));
			employees.add(new Employee(5, "suma", 32, "female", 800000.00, 2));
			employees.add(new Employee(6, "vishnu", 3, "male", 100000.00, 5));
			
			long t1=System.currentTimeMillis();
			employees.stream().filter(e-> e.getSalary()>700000).forEach(System.out::println);
			long t2=System.currentTimeMillis();
			System.out.println("Time taken by sequential stream: "+(t2-t1));
			
			
			long t3=System.currentTimeMillis();
			employees.stream().parallel().filter(e-> e.getSalary()>700000).forEach(System.out::println);
			long t4=System.currentTimeMillis();
			System.out.println("Time taken by parallel stream: "+(t4-t3));
			
			
	}
			

}
