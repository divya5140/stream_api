package com.training.streams.terminal.operations;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class Reduce {

	
	public static void main(String[] args) {
		
		List<Integer> s1=Arrays.asList(1,2,3,4);
		
		List<String> s2=Arrays.asList("divya","sk","diya");
		
		Integer result=s1.stream().reduce(0,(a,b) -> a+b);
		System.out.println(result);
		
		Optional<Integer> results=s1.stream().reduce(Integer::sum);
		System.out.println(results.get());
		
		Optional<String> results1=s2.stream().reduce((a,b) -> a.length()> b.length()?a :b);
		System.out.println(results1.get());
		
	}
}
